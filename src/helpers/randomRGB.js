const UPPER_COLOR_LIMIT = 255;

const randomRGB = () => {
  const mathRound = Math.round;

  return `${mathRound(Math.random() * UPPER_COLOR_LIMIT)},${mathRound(
    Math.random() * UPPER_COLOR_LIMIT,
  )},${mathRound(Math.random() * UPPER_COLOR_LIMIT)}`;
};

export default randomRGB();
