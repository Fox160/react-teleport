export const splitByContinent = (href) => href.split('/continents/')[1].slice(0, -1);

export const getHrefId = (href) => href.split('#')?.pop();

export const getUrbanAreaSlug = (href) => href.split('slug:').pop().split('/')[0];
