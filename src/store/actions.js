import { FETCH_ERROR, FETCH_START, FETCH_SUCCESSED } from './constants';

export const fetchStarted = () => ({
  type: FETCH_START,
  isLoading: true,
});

export const fetchSuccessed = () => ({
  type: FETCH_SUCCESSED,
  isLoading: false,
});

export const fetchError = (err) => ({
  type: FETCH_ERROR,
  isLoading: false,
  error: err,
});
