import {
  applyMiddleware, createStore, combineReducers, compose,
} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import urbanAreas from './urbanAreas';
import cityDetail from './cityDetail';
import fetchUrbanAreas from './urbanAreas/actions';
import { fetchCityDetail, setCityName } from './cityDetail/actions';

const reducers = combineReducers({
  urbanAreas,
  cityDetail,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk, logger)));

export { fetchUrbanAreas };
export { fetchCityDetail, setCityName };

export default store;
