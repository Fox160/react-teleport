import getUrbanAreas from '../../api/cityList';
import FETCH_URBAN_AREAS from './constants';

const fetchUrbanAreas = () => async (dispatch) => {
  const payload = await getUrbanAreas().then((urbanArea) => (
    Promise.allSettled(urbanArea.map((item) => item.then((data) => data)))
  ));
  dispatch({ type: FETCH_URBAN_AREAS, payload: payload.map(({ value }) => value) });
};

export default fetchUrbanAreas;
