import { createReducer } from '@reduxjs/toolkit';
import FETCH_URBAN_AREAS from './constants';

const INITIAL_STATE = [];

const urbanAreas = createReducer(INITIAL_STATE, {
  [FETCH_URBAN_AREAS]: ((state, action) => action.payload),
});

export default urbanAreas;
