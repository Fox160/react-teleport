import { createReducer } from '@reduxjs/toolkit';
import { FETCH_START, FETCH_SUCCESSED, FETCH_ERROR } from '../constants';
import {
  FETCH_CITY_DATA, SET_CITY_NAME,
} from './constants';

const INITIAL_STATE = {};

const cityDetail = createReducer(INITIAL_STATE, {
  [FETCH_CITY_DATA]: ((state, action) => ({ ...state, ...action.payload })),
  [FETCH_START]: (state) => ({ ...state, isLoading: true }),
  [FETCH_SUCCESSED]: (state) => ({ ...state, isLoading: false }),
  [FETCH_ERROR]: (state) => ({ ...state, isLoading: false }),
  [SET_CITY_NAME]: (state, { payload: { name, href } }) => ({ ...state, name, href }),
});

export default cityDetail;
