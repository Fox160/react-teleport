import { SET_CITY_NAME, FETCH_CITY_DATA } from './constants';
import { fetchError, fetchStarted, fetchSuccessed } from '../actions';
import getCityUrbanArea from '../../api/city';

export const fetchCityDetail = (cityName) => (dispatch) => {
  dispatch(fetchStarted());

  getCityUrbanArea(cityName)
    .then((data) => {
      dispatch({
        type: FETCH_CITY_DATA,
        payload: data,
      });
      dispatch(fetchSuccessed());
    })
    .catch((err) => dispatch(fetchError(err)));
};

export const setCityName = ({ name, href }) => ({
  type: SET_CITY_NAME,
  payload: { name, href },
});
