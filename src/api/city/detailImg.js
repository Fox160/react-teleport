import api from '..';

const getDetailImg = (url) => (
  api
    .get(url)
    .then(({ data: { photos } }) => {
      if (photos.length) {
        return photos[0].image;
      }
      return null;
    })
    .catch((err) => ({ type: 'ERROR', msg: err }))
);

export default getDetailImg;
