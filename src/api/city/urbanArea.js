/* eslint-disable no-underscore-dangle */
import api from '..';
import getDetailCategories from './detailCategories';
import getDetailImg from './detailImg';
import getDetailScores from './detailScores';

const getUrbanArea = (url) => (
  api
    .get(url)
    .then(async ({ data }) => {
      const { name, location: mapBound } = data;

      if ((name, mapBound)) {
        const mapCoords = mapBound.latlon;

        return {
          name,
          mapCoords: [mapCoords.latitude, mapCoords.longitude],
          categories: await getDetailCategories(`${data._links[process.env.CITY_URBAN_AREA].href}details`), // TODO разные action'ы
          image: await getDetailImg(`${data._links[process.env.CITY_URBAN_AREA].href}images`),
          scores: await getDetailScores(`${data._links[process.env.CITY_URBAN_AREA].href}scores`),
        };
      }

      return null;
    })
    .catch((err) => ({ type: 'ERROR', msg: err }))
);

export default getUrbanArea;
