import api from '..';

const getDetailScores = (url) => (
  api
    .get(url)
    .then(({ data: { categories, summary } }) => ({ categories, summary }))
    .catch((err) => ({ type: 'ERROR', msg: err }))
);

export default getDetailScores;
