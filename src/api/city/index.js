/* eslint-disable no-underscore-dangle */
import searchCities from './searchCities';
import getUrbanArea from './urbanArea';

const getCityUrbanArea = (cityName) => (
  searchCities({ cityName, embed: `${process.env.CITY_SEARCH_RESULT}/${process.env.CITY_ITEM}` })
    .then((searchResults) => {
      if (searchResults.length) {
        const cityItem = searchResults[0]._links[process.env.CITY_ITEM].href;

        if (cityItem) {
          return getUrbanArea(cityItem);
        }
      }
      return null;
    })
    .catch((err) => ({ type: 'ERROR', msg: err }))
);

export default getCityUrbanArea;
