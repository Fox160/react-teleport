import api from '..';

const getDetailCategories = (url) => (
  api
    .get(url)
    .then(({ data: { categories } }) => categories)
    .catch((err) => ({ type: 'ERROR', msg: err }))
);

export default getDetailCategories;
