import api from '..';

const searchCities = ({ cityName, embed = '' }) => (
  api.get('/cities/', {
    params: {
      search: cityName,
      embed,
    },
  })
    .then(({ data: { _embedded: res } }) => res[process.env.CITY_SEARCH_RESULT])
    .catch((err) => ({ type: 'ERROR', msg: err }))
);

export default searchCities;
