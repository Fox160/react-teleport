/* eslint-disable no-underscore-dangle */
import api from '..';

const getContinents = async () => {
  try {
    const { data } = await api.get('/continents/');

    return data._links[process.env.CONTINENT_ITEMS];
  } catch (err) {
    return { type: 'ERROR', msg: 'Unable to get data' };
  }
};

export default getContinents;
