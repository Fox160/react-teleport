/* eslint-disable no-underscore-dangle */
import getContinents from './continents';
import getUrbanAreasByContinent from './urbanAreasByContinent';

const getUrbanAreas = () => (
  getContinents().then((data) => (
    data.map(({ href, name }) => getUrbanAreasByContinent({ href, name }))
  ))
);

export default getUrbanAreas;
