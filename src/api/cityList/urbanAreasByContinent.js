/* eslint-disable no-underscore-dangle */
import api from '..';
import { splitByContinent } from '../../helpers/hrefSplit';

const getUrbanAreasByContinent = async ({ href, name }) => {
  const continentId = splitByContinent(href);

  try {
    const { data } = await api.get(`/continents/${continentId}/urban_areas/`);
    return {
      continent: name,
      urbanAreas: data._links[process.env.UA_ITEMS],
    };
  } catch (err) {
    return { type: 'ERROR', msg: 'Unable to get data' };
  }
};

export default getUrbanAreasByContinent;
