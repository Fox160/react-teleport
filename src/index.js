import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';

import App from './App';

import store from './store';

import { CustomCookiesProvider } from './utils/cookie';

import './styles/general.scss';

ReactDOM.render(
  <Provider store={store}>
    <CustomCookiesProvider>
      <CookiesProvider>
        <App />
      </CookiesProvider>
    </CustomCookiesProvider>
  </Provider>,
  document.getElementById('root'),
);
