import React, { useEffect } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import CityDetail from './components/DetailPage/CityDetail/CityDetail';
import CityList from './components/ListingPage/CityList/CityList';
import InteractiveMap from './components/MainPage/InteractiveMap/InteractiveMap';
import Header from './components/common/Header/Header';

import { fetchUrbanAreas } from './store';
import CookiesNotification from './components/common/CookiesNotification';

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUrbanAreas());
  }, []);

  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route path="/" exact component={InteractiveMap} />
        <Route path="/city/:cityName" exact component={CityDetail} />
        <Route path="/cities" exact component={CityList} />
      </Switch>
      <CookiesNotification />
    </BrowserRouter>
  );
};

export default App;
