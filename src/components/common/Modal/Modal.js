import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';

import styles from './Modal.modules.scss';

import Close from '../../../assets/img/close.svg';

const Modal = ({
  className, children, defaultOpened, closeModal,
}) => {
  const [isOpen, setIsOpen] = useState(defaultOpened);

  useEffect(() => {
    setIsOpen(defaultOpened);
  }, [defaultOpened]);

  const handleModalClose = () => {
    setIsOpen(false);
    closeModal();
  };

  return (
    isOpen ? (
      ReactDOM.createPortal(
        <div className={classNames(styles.modal, className)}>
          <div className={styles.overlay} onClick={handleModalClose} />

          <div className={styles.body}>
            <div className={styles.close} onClick={handleModalClose} >
              <Close />
            </div>
            {children}
          </div>
        </div>,
        document.getElementById('modal-root'),
      )
    ) : null
  );
};

export default Modal;
