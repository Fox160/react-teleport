import { bool } from 'prop-types';
import React from 'react';

import styles from './Preloader.module.scss';

const Preloader = ({ isLoading }) => {
  if (!isLoading) {
    return null;
  }

  return (
    <div className={styles.preloader}>
      <div className={styles.loader} />
    </div>
  );
};

Preloader.propTypes = {
  isLoading: bool,
};

Preloader.defaultProps = {
  isLoading: false,
};

export default Preloader;
