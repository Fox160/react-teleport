import classNames from 'classnames';
import React from 'react';
import { Link } from 'react-router-dom';
import { useCookies } from 'react-cookie';
import { useDispatch } from 'react-redux';

import { setCityName } from '../../../store';

import HeaderSearch from './HeaderSearch';

import styles from './Header.modules.scss';

import Logo from '../../../assets/img/logo.svg';
import LogoMobile from '../../../assets/img/logo-mob.svg';

const Header = () => {
  const [{ urbanCity }] = useCookies(['urbanCity']);
  const dispatch = useDispatch();

  return (
    <header className={styles.header}>
      <Link to="/cities">All cities</Link>

      {urbanCity && (
        <div className={styles.currentCity}>
          Last selected:{' '}
          <Link
            key={urbanCity.href}
            to={`${urbanCity.href}`}
            className={styles.item}
            onClick={() => dispatch(setCityName({ name: urbanCity.name, href: urbanCity.href }))}
          >
            {urbanCity.name}
          </Link>
        </div>
      )}

      <Link to="/" className={classNames(styles.desktop, styles.logo)}>
        <Logo />
      </Link>
      <Link to="/" className={classNames(styles.mobile, styles.logo)}>
        <LogoMobile />
      </Link>
      <HeaderSearch />
    </header>
  );
};

export default Header;
