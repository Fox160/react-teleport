import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import searchCities from '../../../api/city/searchCities';

import Search from '../Search';

import styles from './Header.modules.scss';

const HeaderSearch = () => {
  const [searchResult, setSearchResult] = useState([]);

  const handleSearchChange = (value) => {
    if (!value) {
      setSearchResult([]);

      return null;
    }

    searchCities({ cityName: value }).then((resultData) => setSearchResult(resultData || []));

    return null;
  };

  return (
    <div className={styles.search}>
      <Search
        modifier="search--header"
        clickableIcon
        handleSearchChange={handleSearchChange}
        searchResult={searchResult}
      />
      {searchResult.length > 0 && (
        <div className={styles.searchResult}>
          {searchResult.map(({ matching_full_name: resultName }) => (
            <Link key={resultName} to={`/city/${resultName.split(', ')[0].toLowerCase()}`}>
              {resultName}
            </Link>
          ))}
        </div>
      )}
    </div>
  );
};

export default HeaderSearch;
