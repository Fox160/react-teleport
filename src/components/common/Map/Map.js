import classNames from 'classnames';
import React, { useEffect, useRef } from 'react';
import { Map, YMaps } from 'react-yandex-maps';
import { string, array } from 'prop-types';

import styles from './Map.modules.scss';

const MAP_STATE = {
  center: [55.75, 37.57],
  zoom: 9,
  controls: ['zoomControl'],
};

const CustomMap = ({ placemark, className }) => {
  const ymaps = useRef(null);
  const placemarkRef = useRef(null);
  const mapRef = useRef(null);

  const addPlacemark = (coords) => {
    if (ymaps.current && mapRef.current) {
      mapRef.current.geoObjects.removeAll();

      placemarkRef.current = new ymaps.current.Placemark(coords);
      mapRef.current.geoObjects.add(placemarkRef.current);
      mapRef.current.setBounds(mapRef.current.geoObjects.getBounds());
      mapRef.current.setZoom(9);
    }
  };

  useEffect(() => {
    addPlacemark(placemark);
  }, [placemark]);

  return (
    <YMaps>
      <Map
        className={classNames(className, styles.map)}
        state={MAP_STATE}
        options={{ maxZoom: 18 }}
        modules={['control.ZoomControl', 'Placemark']}
        instanceRef={mapRef}
        onLoad={(ympasInstance) => {
          ymaps.current = ympasInstance;

          if (placemark) {
            addPlacemark(placemark);
          }
        }}
      />
    </YMaps>
  );
};

CustomMap.propTypes = {
  placemark: array || undefined,
  className: string,
};

CustomMap.defaultProps = {
  placemark: null,
  className: '',
};

export default CustomMap;
