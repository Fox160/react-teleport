import classNames from 'classnames';
import React, { useState } from 'react';
import { string, bool, func } from 'prop-types';

import styles from './Search.modules.scss';

import Loupe from '../../../assets/img/loupe.svg';

const PLACEHOLDER_TEXT = 'Search for a city';

const Search = ({
  placeholder, className, modifier, clickableIcon, handleSearchChange,
}) => {
  const [isExpand, setIsExpand] = useState(false);
  const [inputValue, setInputValue] = useState('');

  const handleSearchClick = () => {
    setIsExpand((prevState) => !prevState);
  };

  const handleChange = (e) => {
    const { value } = e.target;

    setInputValue(value);
    handleSearchChange(value);
  };

  const handleBlur = () => {
    handleSearchChange(null);
  };

  const handleFocus = () => {
    if (inputValue) {
      handleSearchChange(inputValue);
    }
  };

  return (
    <div
      className={classNames(styles.search, className, styles[modifier], {
        [styles.expand]: isExpand,
      })}
    >
      <Loupe
        className={classNames(styles.button, clickableIcon && styles['button--clickable'])}
        onClick={clickableIcon ? handleSearchClick : undefined}
      />
      <input
        placeholder={placeholder || PLACEHOLDER_TEXT}
        className={styles.input}
        onChange={handleChange}
        value={inputValue}
        onBlur={handleBlur}
        onFocus={handleFocus}
      />
    </div>
  );
};

Search.propTypes = {
  placeholder: string,
  className: string,
  modifier: string,
  clickableIcon: bool,
  handleSearchChange: func,
};

Search.defaultProps = {
  placeholder: PLACEHOLDER_TEXT,
  className: '',
  modifier: '',
  clickableIcon: false,
  handleSearchChange: undefined,
};

export default Search;
