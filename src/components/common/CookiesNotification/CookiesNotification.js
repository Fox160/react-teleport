/* eslint-disable max-len */
import React, { useState } from 'react';

import { withCookies } from '../../../utils/cookie';

import styles from './CookiesNotification.modules.scss';
import useCustomCookies from '../../../utils/useCookies/useCookies';

const COOKIE_KEY = 'notification';

const CookiesNotification = () => {
  const [cookie, setCookie] = useCustomCookies(COOKIE_KEY);
  const [cookieState, setCookieState] = useState(cookie);

  const handleBtnCLick = () => {
    setCookie(true);
    setCookieState(true);
  };

  // если кука есть, то просто не рендерим кнопку
  if (cookieState) {
    return null;
  }

  return (
    <div className={styles.cookies}>
      <p>We use cookies to help us give you the best experience on our website. If you continue without changing your settings, we'll assume that you are happy to receive all cookies on our website. However, if you would like to, you can change your cookie settings at any time.</p>
      <button type="button" onClick={handleBtnCLick}>Accept</button>
    </div>
  );
};

export default withCookies(CookiesNotification);
