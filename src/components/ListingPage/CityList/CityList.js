import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import Search from '../../common/Search';
import CityListData from './CityListData';

import styles from './CityList.modules.scss';

const CityList = () => {
  const [searchResult, setSearchResult] = useState([]);
  const [searchValue, setSearchValue] = useState(null);
  const fetchedData = useSelector((state) => state.urbanAreas);

  const handleSearchChange = (value) => {
    setSearchValue(value);

    if (!value) {
      setSearchResult([]);
      return null;
    }

    const res = [];

    fetchedData.forEach(({ continent, urbanAreas }) => {
      const areas = [];

      urbanAreas.forEach((area) => {
        if (area.name.toLowerCase().includes(value.toLowerCase())) {
          areas.push(area);
        }
      });

      res.push({ continent, urbanAreas: areas });
    });

    setSearchResult(res);
    return null;
  };

  const cityListProps = searchValue ? searchResult : fetchedData;

  return (
    <div className={styles['city-list']}>
      <h1 className={styles.title}>Preview Teleport Cities</h1>
      <Search handleSearchChange={handleSearchChange} className={styles.search} />
      <CityListData data={cityListProps} />
    </div>
  );
};

export default CityList;
