import React from 'react';
import { Link } from 'react-router-dom';
import { array } from 'prop-types';
import { useDispatch } from 'react-redux';

import { setCityName } from '../../../store';
import { getUrbanAreaSlug } from '../../../helpers/hrefSplit';

import styles from './CityList.modules.scss';

const CityListData = ({ data }) => {
  const dispatch = useDispatch();

  if (!data.length) {
    return null;
  }

  return (
    <div className={styles.list}>
      {data.map((element) => {
        const { continent, urbanAreas } = element;

        if (!urbanAreas.length) return null;

        return (
          <div className={styles.group} key={continent}>
            {continent && <div className={styles.groupTitle}>{continent}</div>}

            {urbanAreas.map(({ href, name }) => (
              <Link
                key={href}
                to={`/city/${getUrbanAreaSlug(href)}`}
                className={styles.item}
                onClick={() => dispatch(setCityName({ name, href: `/city/${getUrbanAreaSlug(href)}` }))}
              >
                {name}
              </Link>
            ))}
          </div>
        );
      })}
    </div>
  );
};

CityListData.propTypes = {
  data: array || [],
};

CityListData.defaultProps = {
  data: [],
};

export default CityListData;
