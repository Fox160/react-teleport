import React, { useRef, useState } from 'react';
import { useSelector } from 'react-redux';

import CityListData from '../../ListingPage/CityList/CityListData';

import styles from './InteractiveMap.modules.scss';
import stylesCityList from '../../ListingPage/CityList/CityList.modules.scss';

import WorldMap from '../../../assets/img/world-map.svg';
import Modal from '../../common/Modal/Modal';

const InteractiveMap = () => {
  const fetchedData = useSelector((state) => state.urbanAreas);
  const [filteredAreas, setFilteredAreas] = useState([]);
  const mapContainerRef = useRef(null);

  const handleMapClick = ({ target }) => {
    const svgGroups = mapContainerRef.current.querySelectorAll('svg g');
    const targetContinent = target.closest('g');

    if (svgGroups) {
      svgGroups.forEach((element) => {
        element.classList.remove(styles.active);
      });
    }

    if (targetContinent) {
      targetContinent.classList.add(styles.active);

      setFilteredAreas([fetchedData.find(({ continent }) => continent === targetContinent.id)]);
    }
  };

  const closeModal = () => {
    setFilteredAreas([]);
  };

  return (
    <div className={styles['interactive-map']} ref={mapContainerRef}>
      <WorldMap className={styles['interactive-map__map']} onClick={handleMapClick} />
      <Modal defaultOpened={filteredAreas.length > 0} closeModal={closeModal}>
        {filteredAreas.length > 0 && (
          <div className={stylesCityList['city-list']}>
            <CityListData data={filteredAreas} />
          </div>
        )}
      </Modal>
    </div>
  );
};

export default InteractiveMap;
