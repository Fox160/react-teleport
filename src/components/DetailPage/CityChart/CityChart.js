import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { Bar } from 'react-chartjs-2';
import { array, shape } from 'prop-types';

import styles from './CityChart.modules.scss';
import randomRGB from '../../../helpers/randomRGB';

const BACKGROUND_CHART_OPACITY = 0.2;
const BORDER_CHART_OPACITY = 1;
const CHART_OPTIONS = {
  scales: {
    y: {
      beginAtZero: true,
    },
  },
  plugins: {
    legend: {
      display: false,
    },
  },
};
const INITIAL_CHART_DATA = {
  labels: [],
  datasets: [
    {
      data: [],
      backgroundColor: [],
      borderColor: [],
      borderWidth: 1,
    },
  ],
};

const CityChart = ({ data }) => {
  const [chartData, setChartData] = useState(INITIAL_CHART_DATA);

  useEffect(() => {
    if (data) {
      const labels = [];
      const values = [];
      const colors = [];

      data.data.forEach(({ label, int_value: value }) => {
        if (value) {
          labels.push(label);
          values.push(value);
          colors.push(randomRGB);
        }
      });

      setChartData((prevState) => ({
        ...prevState,
        labels,
        datasets: [
          {
            ...prevState.datasets[0],
            data: values,
            backgroundColor: colors.map((color) => `rgba(${color}, ${BACKGROUND_CHART_OPACITY})`),
            borderColor: colors.map((color) => `rgba(${color}, ${BORDER_CHART_OPACITY})`),
          },
        ],
      }));
    }
  }, [data]);

  if (!data) {
    return null;
  }

  return (
    <div className={classNames(styles.chart)}>
      <div className={styles.title}>{data.id}</div>
      <p className={styles.subtitle}>{data.label}</p>
      <Bar data={chartData} options={CHART_OPTIONS} />
    </div>
  );
};

CityChart.propTypes = {
  data: shape({
    data: array || [],
  }),
};

CityChart.defaultProps = {
  data: null,
};

export default CityChart;
