import React from 'react';
import { number, string } from 'prop-types';

import styles from './ScoresDiagram.modules.scss';

const ScoresDiagramRow = ({ name, scores, color }) => (
  <div className={styles.row}>
    <p>{name}</p>
    <div className={styles.line}>
      <div
        style={{
          backgroundColor: color,
          width: `${scores.toFixed(2) * 10}%`,
        }}
      />
    </div>
    <span className={styles.summary}>{parseInt(scores, 10)} / 10</span>
  </div>
);

ScoresDiagramRow.propTypes = {
  name: string,
  scores: number,
  color: string,
};

ScoresDiagramRow.defaultProps = {
  name: '',
  scores: null,
  color: '',
};

export default ScoresDiagramRow;
