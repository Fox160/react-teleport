import React from 'react';
import classNames from 'classnames';
import { array, string } from 'prop-types';

import ScoresDiagramRow from './ScoresDiagramRow';

import styles from './ScoresDiagram.modules.scss';

const ScoresDiagram = ({ data, className }) => {
  if (!data) {
    return null;
  }

  return (
    <div className={classNames(styles.diagram, className)}>
      <div className={styles.title}>Life quality score</div>

      {data.map(({ color, name, score_out_of_10: scores }) => (
        <ScoresDiagramRow key={`${name} ${scores}`} color={color} name={name} scores={scores} />
      ))}
    </div>
  );
};

ScoresDiagram.propTypes = {
  data: array || [],
  className: string,
};

ScoresDiagram.defaultProps = {
  data: [],
  className: '',
};

export default ScoresDiagram;
