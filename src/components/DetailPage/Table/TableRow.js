import { array, string } from 'prop-types';
import React from 'react';

import styles from './Table.modules.scss';

const TableRow = ({ data, className }) => {
  if (!data.length) {
    return null;
  }

  return (
    <tr className={className}>
      {data.map((item) => (
        <td key={item} className={styles.item}>{item}</td>
      ))}
    </tr>
  );
};

TableRow.propTypes = {
  data: array || undefined,
  className: string,
};

TableRow.defaultProps = {
  data: [],
  className: '',
};

export default TableRow;
