import classNames from 'classnames';
import { object, string } from 'prop-types';
import React from 'react';
import useDetailData from '../../../utils/useDetailData/useDetailData';

import styles from './Table.modules.scss';
import TableRow from './TableRow';

const Table = ({ className, data }) => {
  const { detailData: tableData } = useDetailData([], data?.data, [
    'label',
    'currency_dollar_value',
  ]);

  if (!tableData.length) {
    return null;
  }

  return (
    <div className={classNames(styles.tableContainer, className)} id="cost">
      <div className={styles.title}>{data.id}</div>

      {tableData.length && (
        <table className={styles.table}>
          <tbody>
            {tableData.map((row) => {
              const { label, currency_dollar_value: value } = row;

              return (
                <TableRow
                  key={`${label}-${value}`}
                  data={[...Object.values(row)]}
                  className={styles.row}
                />
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
};

Table.propTypes = {
  data: object || undefined,
  className: string,
};

Table.defaultProps = {
  data: undefined,
  className: '',
};

export default Table;
