import React from 'react';
import classNames from 'classnames';

import styles from './CityDetail.modules.scss';

const CityDetailEmpty = () => (
  <div className={classNames(styles.item, styles['item--empty'])}>
    <div className={styles.itemTitle}>
      This city isn&#39;t a Teleport City yet...
    </div>
  </div>
);

export default CityDetailEmpty;
