import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { any } from 'prop-types';
import { useCookies } from 'react-cookie';

import AsideMenu from '../AsideMenu';
import ScoresDiagram from '../ScoresDiagram';
import CityDetailEmpty from './CityDetailEmpty';
import { CityDetailSummary, CityDetailChart, CityDetailLocation } from './CityDetailElement';
import Preloader from '../../common/Preloader/Preloader';

import { fetchCityDetail } from '../../../store';

import styles from './CityDetail.modules.scss';
import CityDetailSlider from '../Slider';
import Table from '../Table';

const ASIDE_MENU_DATA = [
  {
    title: 'Quality of life',
    href: '#summary',
  },
  {
    title: 'Culture',
    href: '#culture',
  },
  {
    title: 'Climate',
    href: '#climate',
  },
  {
    title: 'Cost-of-living',
    href: '#cost',
  },
  {
    title: 'Location',
    href: '#location',
  },
];

const CityDetail = (props) => {
  const dispatch = useDispatch();
  const fetchedData = useSelector((state) => state.cityDetail);
  const [cookies, setCookie] = useCookies(['urbanCity']);

  useEffect(() => {
    const request = fetchedData?.name || props.match.params.cityName;
    dispatch(fetchCityDetail(request));

    if (fetchedData.name) {
      setCookie('urbanCity', JSON.stringify({ name: fetchedData.name, href: fetchedData.href }), {
        path: '/',
      });
    }
  }, []);

  return (
    <div className={styles['city-detail']}>
      <Preloader isLoading={fetchedData.isLoading} />
      {Object.keys(fetchedData).length > 2 ? (
        <>
          <div className={styles.banner}>
            <h1 className={styles.title}>{fetchedData.name}</h1>
            {fetchedData.image ? (
              <>
                <img src={fetchedData.image.mobile} className={styles.mobile} alt="" />
                <img src={fetchedData.image.web} className={styles.desktop} alt="" />
              </>
            ) : null}
          </div>
          <div className={styles.content}>
            <div className={styles.container}>
              <CityDetailSummary data={fetchedData} />

              <ScoresDiagram data={fetchedData.scores?.categories} className={styles.item} />

              <CityDetailChart data={fetchedData} className={styles.item} />

              <CityDetailSlider
                data={fetchedData.categories?.find((category) => category.id === 'CLIMATE')}
                className={styles.item}
              />

              <Table
                data={fetchedData.categories?.find((category) => category.id === 'COST-OF-LIVING')}
                className={styles.item}
              />

              <CityDetailLocation data={fetchedData.mapCoords} />
            </div>
            <AsideMenu className={styles.aside} links={ASIDE_MENU_DATA} />
          </div>
        </>
      ) : (
        <CityDetailEmpty />
      )}
    </div>
  );
};

CityDetail.propTypes = {
  match: any || undefined,
};

CityDetail.defaultProps = {
  match: undefined,
};

export default CityDetail;
