import React from 'react';
import { array, object } from 'prop-types';

import styles from './CityDetail.modules.scss';
import CityChart from '../CityChart';
import CustomMap from '../../common/Map';

// LOCATION
export const CityDetailLocation = ({ data }) => {
  if (!data) {
    return null;
  }

  return (
    <div className={styles.item} id="location">
      <div className={styles.itemTitle}>Location</div>
      <CustomMap placemark={data.length > 0 ? data : null} />
    </div>
  );
};

CityDetailLocation.propTypes = {
  data: array || undefined,
};

CityDetailLocation.defaultProps = {
  data: [],
};

// CHART
export const CityDetailChart = ({ data }) => {
  if (!data) {
    return null;
  }

  return (
    <div className={styles.item} id="culture">
      <CityChart
        data={data.categories?.find((category) => category.id === 'CULTURE')}
      />
    </div>
  );
};

CityDetailChart.propTypes = {
  data: object || undefined,
};

CityDetailChart.defaultProps = {
  data: {},
};

// SUMMARY
export const CityDetailSummary = ({ data }) => {
  if (!data) {
    return null;
  }

  const { name, scores } = data;

  return (
    <div className={styles.item} id="summary">
      <div className={styles.itemTitle}>Quality of life in {name}</div>
      <div
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{
          __html: scores?.summary,
        }}
      />
    </div>
  );
};

CityDetailSummary.propTypes = {
  data: object || undefined,
};

CityDetailSummary.defaultProps = {
  data: {},
};
