import React, { useEffect } from 'react';
import { object, string } from 'prop-types';
import classNames from 'classnames';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation } from 'swiper';

import 'swiper/swiper-bundle.min.css';
import styles from './Slider.modules.scss';

import Sun from '../../../assets/img/sun.svg';

import useDetailData from '../../../utils/useDetailData/useDetailData';

SwiperCore.use([Navigation]);

const CityDetailSlider = ({ className, data }) => {
  const { detailData: sliderData } = useDetailData([], data.data, ['label', 'float_value']);

  return (
    <div className={classNames(className, styles.slider)} id="climate">
      <div className={styles.title}>{data.id}</div>

      {sliderData.length ? (
        <Swiper navigation>
          {sliderData.map(({ label, float_value: value }) => (
            <SwiperSlide key={label}>
              <div className={styles.slide}>
                <div className={styles.slideTitle}>{label}</div>
                <div className={styles.slideRow}>
                  <div>{value}</div>
                  <Sun />
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      ) : null}
    </div>
  );
};

CityDetailSlider.propTypes = {
  data: object || undefined,
  className: string,
};

CityDetailSlider.defaultProps = {
  data: {},
  className: '',
};

export default CityDetailSlider;
