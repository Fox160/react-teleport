import React, { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import { array, string } from 'prop-types';
import { HashLink } from 'react-router-hash-link';

import styles from './AsideMenu.modules.scss';
import { getHrefId } from '../../../helpers/hrefSplit';

const AsideMenu = ({ links, className }) => {
  const asideMenuRef = useRef();
  const [activeScrollId, setActiveScrollId] = useState('');

  useEffect(() => {
    const menuItems = [...asideMenuRef.current.querySelectorAll('a')];

    const scrollActive = () => {
      let currentId;

      menuItems.forEach((item) => {
        const scrollItem = document.getElementById(getHrefId(item.href));

        if (scrollItem?.offsetTop <= window.scrollY) {
          currentId = scrollItem.id;
        }
      });

      if (currentId && activeScrollId !== currentId) {
        setActiveScrollId(currentId);
      }
    };

    window.addEventListener('scroll', scrollActive);

    return () => window.removeEventListener('scroll', scrollActive);
  }, []);

  if (!links.length) {
    return null;
  }

  return (
    <ul className={classNames(styles['aside-menu'], className)} ref={asideMenuRef}>
      {links.map((item) => {
        const isActive = getHrefId(item.href) === activeScrollId;

        return (
          <li key={item.href} className={classNames({ [styles.active]: isActive })}>
            <HashLink to={item.href}>{item.title}</HashLink>
          </li>
        );
      })}
    </ul>
  );
};

AsideMenu.propTypes = {
  links: array || [],
  className: string,
};

AsideMenu.defaultProps = {
  links: [],
  className: '',
};

export default AsideMenu;
