import { useEffect, useState } from 'react';

const useDetailData = (initialState = [], newData, fields) => {
  const [detailData, setDetailData] = useState(initialState);

  useEffect(() => {
    if (!newData) return;

    setDetailData(newData.reduce((result, item) => {
      const newValue = fields.reduce((res, field) => {
        if (item[field]) {
          return { ...res, [field]: item[field].toString() };
        }

        return res;
      }, {});

      if (Object.keys(newValue).length === fields.length) {
        result.push(newValue);
      }
      return result;
    }, []));
  }, [newData]);

  return { detailData };
};

export default useDetailData;
