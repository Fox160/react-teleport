import { useContext } from 'react';
import CustomCookiesContext from '../cookie/Context';

const useCustomCookies = (name) => {
  // С помощью хука useContext вытаскиваем менеджер/провайдер из контекста
  const manager = useContext(CustomCookiesContext);

  // Если менеджера нет, то кидаем исключение
  if (!manager) {
    throw new Error('Missing <CookiesProvider>');
  }

  // Если в аргументах передали название куки, то возвращаем ее значение
  // И метод для установки значения для куки с переданным названием
  if (name) {
    return [manager.get(name), manager.set.bind(manager, name)];
  }

  // Иначе возвращаем объект со всеми куками
  return [manager.getAll(), manager.set.bind(manager)];
};

export default useCustomCookies;
