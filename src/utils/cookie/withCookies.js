/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import CookiesContext from './Context';

const withCookies = (ComposedComponent) => {
  const name = ComposedComponent.displayName || ComposedComponent.name;

  class CokiesCLass extends Component {
    render() {
      return (
        <CookiesContext.Consumer>
          {(manager) => (
            <ComposedComponent
              cookies={manager.getAll()}
              setCookie={manager.set.bind(manager)}
              {...this.props}
            />
          )}
        </CookiesContext.Consumer>
      );
    }
  }

  CokiesCLass.displayName = `withCookies(${name})`;

  return CokiesCLass;
};

export default withCookies;
