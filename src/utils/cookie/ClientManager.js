/* eslint-disable class-methods-use-this */
export default class ClientManager {
  get(name) {
    return this.getAll()[name];
  }

  getAll() {
    const pairs = document.cookie.split(';');

    return pairs.reduce((res, current) => {
      const pair = current.split('=');
      res[(`${pair[0]}`).trim()] = decodeURIComponent(pair[1]);

      return res;
    }, {});
  }

  set(name, value = '', days = 30) {
    const date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

    const chunks = [
      `${name}=${value}`,
      `expires=${date.toUTCString()}`,
      'path=/',
    ].join('; ');

    document.cookie = chunks;
  }
}
