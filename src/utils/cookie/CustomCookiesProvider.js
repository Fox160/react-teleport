import React from 'react';
import { arrayOf, node, oneOfType } from 'prop-types';

import CookiesContext from './Context';
import ClientManager from './ClientManager';

const CustomCookiesProvider = ({ manager, children }) => (
  <CookiesContext.Provider value={manager}>
    {children}
  </CookiesContext.Provider>
);

CustomCookiesProvider.propTypes = {
  manager: new ClientManager(),
  children: oneOfType([
    arrayOf(node),
    node,
  ]).isRequired,
};

CustomCookiesProvider.defaultProps = {
  manager: new ClientManager(),
};

export default CustomCookiesProvider;
