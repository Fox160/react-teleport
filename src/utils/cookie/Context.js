import React from 'react';

const CustomCookiesContext = React.createContext('cookies');

export default CustomCookiesContext;
